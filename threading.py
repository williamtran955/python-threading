import time, threading

def thread1():
    x = 0
    while x < 11:
        print(x)
        x += 1
        time.sleep(1)

def thread2():
    y = 10
    while y > -1:
        print(y)
        y -= 1
        time.sleep(1)

if __name__ == "__main__":
    threads=list()
    
    t1 = threading.Thread(target=thread1)
    threads.append(t1)
    t2 = threading.Thread(target=thread2)
    threads.append(t2)
    t1.start()
    t2.start()
    t1.join()
    t2.join()